//#![feature(associated_type_defaults)]

#![recursion_limit="1024"]

#[macro_use]
extern crate lazy_static;



#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;

// Imports used by tests
#[cfg(test)]
#[macro_use]
extern crate assert_matches;
#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;
#[cfg(test)]
// Modules used by tests
#[macro_use]
mod test_macros;


pub mod file_list;
pub mod persistent_file_list;
pub mod file_list_worker;
pub mod file_database;
pub mod settings;
pub mod search_handler;
pub mod file_util;
pub mod file_request_handlers;
pub mod exiftool;
pub mod search;
pub mod date_search;
pub mod schema;
pub mod request_helpers;
pub mod file_list_response;
pub mod error;
pub mod changelog;
pub mod sync;
pub mod sync_handlers;
pub mod sync_progress;
pub mod util;
pub mod file_handler;
pub mod byte_source;
pub mod foreign_server;
pub mod misc_handlers;

pub mod fix_timestamps;
pub mod db_fixes;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

