use thiserror::Error;

use std::path::PathBuf;

use iron::{IronError, status, Response};
use std::convert;

#[derive(Error, Debug)]
pub enum Error {
    // foreign errors
    #[error("Error parsing exif data")]
    Exif(#[from] crate::exiftool::Error),
    #[error("Io error")]
    Io(#[from] ::std::io::Error),
    #[error("SerdeJson error")]
    SerdeJson(#[from] ::serde_json::Error),
    #[error("Diesel error")]
    Diesel(#[from] ::diesel::result::Error),
    #[error("DieselConnection error")]
    DieselConnection(#[from] ::diesel::ConnectionError),
    #[error("ImageError error")]
    ImageError(#[from] ::image::ImageError),
    #[error("Utf8 error")]
    Utf8(#[from] ::std::string::FromUtf8Error),
    #[error("StrUtf8 error")]
    StrUtf8(#[from] ::std::str::Utf8Error),
    #[error("Reqwest error")]
    Reqwest(#[from] ::reqwest::Error),
    #[error("SystemTime error")]
    SystemTime(#[from] ::std::time::SystemTimeError),


    // Local errors
    #[cfg(test)]
    #[error("Dummy error")]
    Dummy,
    #[error("Failed to read persistent file list")]
    PersistentFileListLoadError,

    #[error("Path {0:?} does not have an extension")]
    NoFileExtension(PathBuf),

    // Errors relating to url variable parsing 
    #[error("No variable named {0}")]
    NoSuchVariable(String),
    #[error("Variable {0} exists but is not {1}")]
    InvalidVariableType(String, String),
    #[error("The given URL contains no url encoded query")]
    NoUrlEncodedQuery,
    #[error("Unrecognised action {0}")]
    UnknownAction(String),


    // Intermediate errors
    #[error("Byte source expansion failed")]
    ByteSourceExpansionFailed(#[source] Box<Error>),

    // File handling errors
    #[error("File {0} could not be removed from database")]
    FileDatabaseRemovalFailed(i32, #[source] Box<Error>),
    #[error("File {0} could not be removed")]
    FileRemovalFailed(String, #[source] ::std::io::Error),

    #[error("Failed to generate thumbnail")]
    ThumbnailGenerationFailed(#[source] Box<Error>),


    // Errors specific to file requests
    #[error("no file list with id {0}")]
    NoSuchList(usize),
    #[error("No file with id {0} in list {1}")]
    NoSuchFileInList(usize, usize),

    // Database errors
    #[error("The database did not contain a file with id {0}")]
    NoSuchFileInDatabase(i32),
    #[error("An ID collision occured when inserting a change with id {0}")]
    ChangeIDCollision(i32),

    // Foreign server errors
    #[error("Foreign server communication failed. Url: {0}")]
    ForeignHttpError(String),

    #[error("HTTP request returned status {0}. Response: {1}")]
    WrongHttpStatusCode(::reqwest::StatusCode, String),

    #[error("No job with id {0}")]
    NoSuchJobId(usize),

    #[error("{0}")]
    PlainMessage(String, #[source] Box<Error>)
}

pub trait ResultExt {
    fn trace_info<X>(self, msg: impl Fn() -> X) -> Self 
        where X: Into<String>;
}

impl<T> ResultExt for Result<T> {
    fn trace_info<X>(self, msg: impl Fn() -> X) -> Self 
        where X: Into<String>
    {
        self.map_err(|e| Error::PlainMessage(msg().into(), Box::new(e)))
    }
}


impl Error {
    fn iron_status(&self) -> status::Status {
        match *self {
            Error::NoSuchVariable(_) |
            Error::InvalidVariableType(_, _) |
            Error::NoUrlEncodedQuery => status::Status::BadRequest,
            Error::UnknownAction(_) |
            Error::NoSuchList(_) |
            Error::NoSuchFileInList(_, _) |
            Error::NoSuchFileInDatabase(_) => status::Status::NotFound,
            _ => status::Status::InternalServerError
        }
    }
}


impl convert::From<Error> for IronError {
    fn from(source: Error) -> IronError {
        let message = format!("{:#?}\n", source);

        let status = source.iron_status();
        IronError {
            error: Box::new(source),
            response: Response::with((status, message)),
        }
    }
}


pub type Result<T> = ::std::result::Result<T, Error>;
