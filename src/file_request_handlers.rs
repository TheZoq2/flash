
use serde_json;

use iron::*;
use persistent::Write;

use std::path::{Path, PathBuf};

use std::sync::Mutex;
use std::sync::Arc;

use chrono::{NaiveDateTime, Utc};

use crate::file_database;
use crate::file_database::FileDatabase;
use crate::file_list::{FileListList, FileLocation};
use crate::file_list_worker;
use crate::persistent_file_list;
use crate::file_util::{
    get_semi_unique_identifier,
    get_file_timestamp,
    sanitize_tag_names
};
use crate::request_helpers::{get_get_variable, setup_db_connection};
use crate::file_handler::{save_file, FileSavingWorkerResults, ThumbnailStrategy};
use crate::byte_source::ByteSource;
use crate::changelog;
use crate::changelog::ChangeCreationPolicy;

use crate::file_list_response;

use crate::error::{Result, Error, ResultExt};

enum FileAction {
    GetData,
    GetFile,
    GetFilename,
    GetThumbnail,
    Save
}

impl FileAction {
    fn try_parse(action: &str) -> Option<FileAction>
    {
        match action {
            "get_data" => Some(FileAction::GetData),
            "get_file" => Some(FileAction::GetFile),
            "get_filename" => Some(FileAction::GetFilename),
            "get_thumbnail" => Some(FileAction::GetThumbnail),
            "save" => Some(FileAction::Save),
            _ => None
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
//                      Helper types used for passing
//                      response data between functions
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub enum RawFileSource {
    Path(PathBuf),
    Database(i32)
}

#[derive(Serialize, Deserialize)]
pub struct FileData {
    pub file_path: String,
    pub thumbnail_path: String,
    pub tags: Vec<String>,
    pub creation_date: NaiveDateTime,
    pub raw_file: Option<RawFileSource>,
}

impl FileData {
    fn from_database(source: file_database::File) -> FileData {
        FileData {
            file_path: source.filename,
            thumbnail_path: source.thumbnail_path.unwrap_or_else(|| String::from("")),
            tags: source.tags,
            creation_date: source.creation_date,
            raw_file: source.raw_file.map(|id| RawFileSource::Database(id))
        }
    }

    fn from_path(source: &Path, raw_file: &Option<PathBuf>) -> Result<FileData> {
        Ok(FileData {
            file_path: String::from(source.to_string_lossy()),
            thumbnail_path: String::from(source.to_string_lossy()),
            tags: vec![],
            creation_date: get_file_timestamp(&source)
                .trace_info(|| "Failed to read file timestamp")?,
            raw_file: raw_file.as_ref().map(|path| RawFileSource::Path(path.clone()))
        })
    }
}

#[derive(Debug)]
enum FileSaveRequestResult {
    NewDatabaseEntry(FileLocation, FileSavingWorkerResults),
    UpdatedDatabaseEntry(FileLocation),
}


////////////////////////////////////////////////////////////////////////////////
//                      Public request handlers
////////////////////////////////////////////////////////////////////////////////

/**
  Handles requests for actions dealing with specific entries in file lists
*/
pub fn file_list_request_handler(request: &mut Request) -> IronResult<Response> {
    let action_str = get_get_variable(request, "action")?;

    if let Some(action) = file_list_response::GlobalAction::try_parse(&action_str) {
        file_list_response::global_list_action_handler(request, &action)
    }
    else if let Some(action) = file_list_response::ListAction::try_parse(&action_str) {
        file_list_response::list_action_handler(request, &action)
    }
    else if let Some(action) = FileAction::try_parse(&action_str) {
        file_request_handler(request, &action)
    }
    else {
        Err(Error::UnknownAction(action_str))?
    }
}

/**
  Handles requests for actions dealing with specific entries in file lists
*/
fn file_request_handler(request: &mut Request, action: &FileAction) -> IronResult<Response> {
    let (list_id, file_index) = read_request_list_id_index(request)?;
    let fdb = setup_db_connection(request)?;

    let file_location = {
        let mutex = request.get::<Write<FileListList>>().unwrap();
        let file_list_list = mutex.lock().unwrap();

        get_file_list_object(&*file_list_list, list_id, file_index)?
    };

    let file_storage_folder = fdb.get_file_save_path();

    match *action {
        FileAction::GetData => {
            let file_data = file_data_from_file_location(&file_location)?;
            Ok(Response::with(
                (status::Ok, serde_json::to_string(&file_data).unwrap()),
            ))
        }
        FileAction::GetFile => {
            let path = get_file_location_path(&file_storage_folder, &file_location);
            Ok(Response::with((status::Ok, path)))
        }
        // TODO: Don't return an absolute path for unsaved files. Do we even need the path? (maybe
        // in flash_cli)
        FileAction::GetFilename => {
            let path = match file_location {
                FileLocation::Database(entry) => entry.filename,
                FileLocation::Unsaved{path, raw_file: _} => {
                    String::from(path.to_string_lossy())
                }
            };
            Ok(Response::with((status::Ok, path)))
        }
        FileAction::GetThumbnail => {
            let path = get_file_list_thumbnail(&file_storage_folder, &file_location);
            Ok(Response::with((status::Ok, path)))
        }
        FileAction::Save => {
            let tags = get_tags_from_request(request)?;
            let current_time = NaiveDateTime::from_timestamp_opt(Utc::now().timestamp(), 0).unwrap();
            match handle_save_request(&fdb, &file_location, &tags, current_time)? {
                FileSaveRequestResult::NewDatabaseEntry(new_location, _) |
                FileSaveRequestResult::UpdatedDatabaseEntry(new_location) => {
                    let mut file_list_list = request.get::<Write<FileListList>>().unwrap();
                    update_file_list(&mut file_list_list, list_id, file_index, &new_location);

                    send_file_list_save_command(request);

                    Ok(Response::with((status::Ok, "\"ok\"")))
                }
            }
        }
    }
}



////////////////////////////////////////////////////////////////////////////////
///                     Private functions for getting data
///                     out of iron requests
////////////////////////////////////////////////////////////////////////////////

fn read_request_list_id_index(request: &mut Request) -> Result<(usize, usize)> {
    let list_id = file_list_response::read_request_list_id(request)?;

    let file_index = get_get_variable(request, "index")?;

    let file_index = match file_index.parse::<usize>() {
        Ok(val) => val,
        Err(_) => {
            return Err(Error::InvalidVariableType("index".into(), "usize".into()).into());
        }
    };

    Ok((list_id, file_index))
}



fn get_tags_from_request(request: &mut Request) -> Result<Vec<String>> {
    //Get the important information from the request.
    let tag_string = get_get_variable(request, "tags")?;

    match serde_json::from_str::<Vec<String>>(&tag_string) {
        Ok(result) => Ok(sanitize_tag_names(&result)),
        Err(e) => Err(Error::InvalidVariableType("tags".into(), format!("{:?}", e)).into()),
    }
}


fn send_file_list_save_command(request: &mut Request) {
    let file_list_list = request.get::<Write<FileListList>>().unwrap();

    // Save the current file lists to disk
    let saveable_file_list = {
        let fll = file_list_list.lock().unwrap();

        persistent_file_list::saveable_file_list_list(&fll)
    };

    let flw = request.get::<Write<file_list_worker::Commander>>().unwrap();

    flw.lock()
        .unwrap()
        .send(file_list_worker::Command::Save(saveable_file_list))
        .unwrap();
}


////////////////////////////////////////////////////////////////////////////////
//                      Private functions for generating
//                      replies to file requests
////////////////////////////////////////////////////////////////////////////////

/**
  Updates the specified `file_list` with a new `FileLocation`
*/
fn update_file_list(
    file_list_list: &mut Arc<Mutex<FileListList>>,
    list_id: usize,
    file_index: usize,
    new_location: &FileLocation,
) {
    let mut file_list_list = file_list_list.lock().unwrap();
    file_list_list.edit_file_list_entry(list_id, file_index, new_location);
}

/**
  Saves the specified tags for the file. If a new `FileLocation` has been created,
  it is returned. Otherwise None. If saving failed an error is returned
*/
fn handle_save_request(
    db: &FileDatabase,
    file_location: &FileLocation,
    tags: &[String],
    change_timestamp: NaiveDateTime
) -> Result<FileSaveRequestResult> {
    match file_location {
        FileLocation::Unsaved{path, raw_file: raw_path} => {
            let file_timestamp = get_file_timestamp(path)?;

            match save_new_file(db, path, tags, file_timestamp, change_timestamp, raw_path) {
                Ok((db_entry, save_result_rx)) => {
                    Ok(FileSaveRequestResult::NewDatabaseEntry(
                        FileLocation::Database(db_entry),
                        save_result_rx,
                    ))
                }
                Err(e) => Err(e),
            }
        }
        FileLocation::Database(ref old_file) => {
            match update_stored_file_tags(db, old_file, tags, change_timestamp) {
                Ok(db_entry) => {
                    Ok(FileSaveRequestResult::UpdatedDatabaseEntry(
                        FileLocation::Database(db_entry),
                    ))
                }
                Err(e) => Err(e),
            }
        }
    }
}

/**
  Saves a specified file in the `Filedatabase`
*/
fn save_new_file(
    db: &FileDatabase,
    original_path: &Path,
    tags: &[String],
    file_timestamp: NaiveDateTime,
    current_time: NaiveDateTime,
    raw_file: &Option<PathBuf>
) -> Result<(file_database::File, FileSavingWorkerResults)> {
    let file_identifier = get_semi_unique_identifier();

    let file = ByteSource::File(original_path.into());

    let extension = match original_path.extension() {
        Some(val) => Ok(val),
        None => Err(Error::NoFileExtension(original_path.to_owned()))
    }?;

    let raw_id = if let Some(raw_file) = raw_file {
        // Save the raw file first without any tags
        let raw_identifier = get_semi_unique_identifier();
        let extension = match raw_file.extension() {
            Some(val) => Ok(val),
            None => Err(Error::NoFileExtension(original_path.to_owned()))
        }?;
        let save_result = save_file(
            ByteSource::File(raw_file.into()),
            ThumbnailStrategy::None,
            raw_identifier,
            &vec!["__raw".to_string()],
            db,
            &changelog::ChangeCreationPolicy::Yes(current_time),
            &extension.to_string_lossy(),
            file_timestamp.timestamp() as u64,
            None,
        )?;
        Some(save_result.0.id)
    } else {
        None
    };


    save_file(
        file,
        ThumbnailStrategy::Generate,
        file_identifier,
        tags,
        db,
        &changelog::ChangeCreationPolicy::Yes(current_time),
        &extension.to_string_lossy(),
        file_timestamp.timestamp() as u64,
        raw_id,
    )
}


/**
  Updates a specified file in the database with new tags
*/
fn update_stored_file_tags(
    db: &FileDatabase,
    old_entry: &file_database::File,
    tags: &[String],
    change_timestamp: NaiveDateTime
) -> Result<file_database::File> {
    db.change_file_tags(old_entry, tags, &ChangeCreationPolicy::Yes(change_timestamp))
}


/**
  Returns a `FileData` struct for the specified file location
*/
fn file_data_from_file_location(file: &FileLocation) -> Result<FileData> {
    // Lock the file list and try to fetch the file
    Ok(match file {
        FileLocation::Unsaved{path, raw_file: raw_path} => FileData::from_path(path, raw_path)?,
        FileLocation::Database(db_entry) => FileData::from_database(db_entry.clone()),
    })
}

/**
  Returns a the path to a `FileLocation`
*/
fn get_file_location_path(storage_folder: &Path, file: &FileLocation) -> PathBuf {
    match *file {
        FileLocation::Unsaved{ref path, raw_file: _} => path.clone(),
        FileLocation::Database(ref db_entry) => {
            storage_folder.join(db_entry.filename.clone())
        }
    }
}

/**
  Returns the path to the thumbnail of a `FileLocation`
*/
fn get_file_list_thumbnail(storage_folder: &Path, file: &FileLocation) -> PathBuf {
    match *file {
        FileLocation::Unsaved{ref path, raw_file: _} => path.clone(),
        FileLocation::Database(ref db_entry) => {
            storage_folder.join(
                db_entry.thumbnail_path.clone().unwrap_or_else(|| String::from(""))
            )
        }
    }
}

/**
  Returns a `FileLocation` from a `FileListList`, a list id and a file id
*/
fn get_file_list_object(
    file_list_list: &FileListList,
    list_id: usize,
    file_index: usize,
) -> Result<FileLocation> {
    let file_list = match file_list_list.get(list_id) {
        Some(list) => list,
        None => {
            return Err(Error::NoSuchList(list_id).into());
        }
    };

    match file_list.get(file_index) {
        Some(file) => Ok(file.clone()),
        None => Err(Error::NoSuchFileInList(list_id, file_index).into()),
    }
}

/*
  Database tests are not currently run because the database can't be shared
  between test threads
*/
#[cfg(test)]
mod file_request_tests {
    use super::*;

    use crate::search;
    use crate::date_search;

    use crate::file_list::{FileList, FileListSource};

    use chrono::NaiveDate;

    use crate::changelog::{Change, ChangeType, ChangeCreationPolicy};

    fn dummy_database_entry(file_path: &str, thumbnail_path: &str) -> file_database::File {
        file_database::File {
            id: 0,
            filename: file_path.to_owned(),
            raw_file: None,
            thumbnail_path: Some(thumbnail_path.to_owned()),
            creation_date: NaiveDate::from_ymd_opt(2016,1,1).unwrap().and_hms_opt(0,0,0).unwrap(),
            is_uploaded: true,
            tags: vec![],
        }
    }

    fn make_dummy_file_list_list() -> FileListList {
        let mut fll = FileListList::new();

        let flist1 = FileList::from_locations(
            vec![
                FileLocation::Unsaved{path: PathBuf::from("l0f0"), raw_file: None},
                FileLocation::Unsaved{path: PathBuf::from("l0f1"), raw_file: None},
                FileLocation::Unsaved{path: PathBuf::from("l0f2"), raw_file: None},
            ],
            FileListSource::Search,
        );

        let flist2 = FileList::from_locations(
            vec![
                FileLocation::Unsaved{path: PathBuf::from("l1f0"), raw_file: None},
                FileLocation::Unsaved{path: PathBuf::from("l1f1"), raw_file: None},
            ],
            FileListSource::Search,
        );

        let flist3 = FileList::from_locations(
            vec![
                FileLocation::Database(dummy_database_entry("test1", "thumb1")),
                FileLocation::Database(dummy_database_entry("test2", "thumb2")),
            ],
            FileListSource::Search,
        );

        fll.add(flist1);
        fll.add(flist2);
        fll.add(flist3);

        fll
    }

    #[test]
    fn file_list_object_test() {
        let fll = make_dummy_file_list_list();

        // Getting files from the first list works
        assert_eq!(
            get_file_list_object(&fll, 0, 0).unwrap(),
            FileLocation::Unsaved{path: PathBuf::from("l0f0"), raw_file: None}
        );
        assert_eq!(
            get_file_list_object(&fll, 0, 2).unwrap(),
            FileLocation::Unsaved{path: PathBuf::from("l0f2"), raw_file: None}
        );
        // Getting files from the second list works
        assert_eq!(
            get_file_list_object(&fll, 1, 1).unwrap(),
            FileLocation::Unsaved{path: PathBuf::from("l1f1"), raw_file: None}
        );

        //Out of bounds
        assert!(get_file_list_object(&fll, 0, 3).is_err());
        assert!(get_file_list_object(&fll, 1, 2).is_err());
        assert!(get_file_list_object(&fll, 3, 2).is_err());
    }

    #[test]
    fn updating_file_list_entries_works() {
        let mut fll = Arc::new(Mutex::new(make_dummy_file_list_list()));

        let new_db_entry = FileLocation::Database(dummy_database_entry("yolo", "swag"));

        update_file_list(&mut fll, 0, 0, &new_db_entry);

        let fll = fll.lock().unwrap();
        assert_matches!(get_file_list_object(&fll, 0, 0).unwrap()
                        , FileLocation::Database(_))
    }


    // TODO: Use db_test! instead of this test runner
    #[test]
    fn database_related_tests() {
        let fdb = file_database::db_test_helpers::get_database();
        let fdb = fdb.lock().unwrap();

        fdb.reset();
        saving_a_file_without_extension_fails(&fdb);

        fdb.reset();
        file_list_saving_works(&fdb);

        fdb.reset();
        file_list_updates_work(&fdb);

        fdb.reset();
        file_list_save_requests_work(&fdb);

        fdb.reset();
        saving_a_raw_file_works(&fdb);
    }

    fn saving_a_file_without_extension_fails(fdb: &FileDatabase) {
        let tags = vec!("test1".to_owned(), "test2".to_owned());
        assert_matches!(
                save_new_file(
                    fdb,
                    &PathBuf::from("test"),
                    &tags,
                    NaiveDate::from_ymd_opt(2017,1,1).unwrap().and_hms_opt(0,0,0).unwrap(),
                    NaiveDate::from_ymd_opt(2017,1,1).unwrap().and_hms_opt(0,0,0).unwrap(),
                    &None
                ),
                Err(Error::NoFileExtension(_))
            );
    }

    fn file_list_saving_works(fdb: &FileDatabase) {
        let tags = vec!("test1".to_owned(), "test2".to_owned());

        let src_path = PathBuf::from("test/media/10x10.png");
        let (result, worker_results) = save_new_file(
                &fdb,
                &src_path,
                &tags,
                NaiveDate::from_ymd_opt(2017, 1, 1).unwrap().and_hms_opt(0,0,0).unwrap(),
                NaiveDate::from_ymd_opt(2017, 1, 1).unwrap().and_hms_opt(0,0,0).unwrap(),
                &None
            )
            .unwrap();


        let save_result = worker_results.file.recv().unwrap();
        assert_matches!(save_result, Ok(()));
        let thumbnail_save_result = worker_results.thumbnail.unwrap().recv().expect("Thumbnail creator crashed");
        assert_matches!(thumbnail_save_result, Ok(()));

        let full_path = {
            PathBuf::from(fdb.get_file_save_path()).join(PathBuf::from(&result.filename))
        };

        // Make sure that the saved file exists
        assert!(full_path.exists());

        //Make sure that the file was actually added to the database
        assert!(
            fdb.search_files(search::SavedSearchQuery::with_tags((tags, vec!())))
                .iter()
                .fold(false, |acc, file| { acc || file.id == result.id })
            )
    }

    fn file_list_save_requests_work(fdb: &FileDatabase) {
        let old_path = PathBuf::from("test/media/10x10.png");

        let tags = vec!("new1".to_owned());

        let saved_entry = {
            let timestamp = NaiveDate::from_ymd_opt(2017,1,1).unwrap().and_hms_opt(0,0,0).unwrap();
            let result = handle_save_request(
                fdb,
                &FileLocation::Unsaved{path: old_path, raw_file: None},
                &tags,
                timestamp
            );

            assert_matches!(result, Ok(_));
            let result = result.unwrap();

            assert_matches!(result, FileSaveRequestResult::NewDatabaseEntry(FileLocation::Database(_), _));
            match result {
                FileSaveRequestResult::NewDatabaseEntry(file_entry, worker_receivers) => {
                    assert_matches!(worker_receivers.file.recv().unwrap(), Ok(()));
                    assert_matches!(worker_receivers
                            .thumbnail
                            .expect("Thumbnail generator channel not created")
                            .recv()
                            .expect("Thumbnail saving failed"), Ok(())
                        );

                    match file_entry {
                        FileLocation::Database(result) => {
                            assert!(result.tags.contains(&String::from("new1")));
                            assert!(!result.tags.contains(&String::from("old")));
                            result
                        }
                        _ => panic!("Unreachable branch"),
                    }
                }
                _ => panic!("Unreachable branch"),
            }
        };

        //Make sure that the file was actually added to the database
        assert!(
                fdb
                    .search_files(search::SavedSearchQuery::with_tags((tags, vec!())))
                    .iter()
                    .fold(false, |acc, file| { acc || file.id == saved_entry.id })
            );
    }

    fn file_list_updates_work(fdb: &FileDatabase) {
        let old_tags = vec!(String::from("old"));
        let old_location = {
            fdb.add_new_file(
                1,
                "test",
                Some("thumb"),
                None,
                &old_tags,
                0,
                &ChangeCreationPolicy::No
            )
        };

        let tags = vec!("new1".to_owned());

        let saved_entry = {
            let timestamp = NaiveDate::from_ymd_opt(2017,1,1).unwrap().and_hms_opt(0,0,0).unwrap();

            let result =
                handle_save_request(
                    &fdb,
                    &FileLocation::Database(old_location),
                    &tags,
                    timestamp
                );

            assert_matches!(result, Ok(_));
            let result = result.unwrap();

            assert_matches!(result, FileSaveRequestResult::UpdatedDatabaseEntry(FileLocation::Database(_)));
            match result {
                FileSaveRequestResult::UpdatedDatabaseEntry(result) => {
                    match result {
                        FileLocation::Database(result) => {
                            assert!(result.tags.contains(&String::from("new1")));
                            assert!(!result.tags.contains(&String::from("old")));
                            result
                        }
                        _ => panic!("Unreachable branch"),
                    }
                }
                _ => panic!("Unreachable branch"),
            }
        };

        // Make sure that the file was actually added to the database
        assert!(
                fdb
                    .search_files(search::SavedSearchQuery::with_tags((tags, vec!())))
                    .iter()
                    .fold(false, |acc, file| { acc || file.id == saved_entry.id })
            );

        // Make sure the old entry was removed
        assert!(
                fdb
                    .search_files(search::SavedSearchQuery::with_tags((old_tags, vec!())))
                    .iter()
                    .fold(false, |acc, file| { acc || file.id == saved_entry.id })
                ==
                false
            );
    }


    fn saving_a_raw_file_works(fdb: &FileDatabase) {
        let tags = vec!("test1".to_owned(), "test2".to_owned());

        let src_path = PathBuf::from("test/media/10x10.png");
        let (result, _) = save_new_file(
                &fdb,
                &src_path,
                &tags,
                NaiveDate::from_ymd_opt(2017, 1, 1).unwrap().and_hms_opt(0,0,0).unwrap(),
                NaiveDate::from_ymd_opt(2017, 1, 1).unwrap().and_hms_opt(0,0,0).unwrap(),
                &Some("test/media/test.nef".into())
            )
            .unwrap();

        assert!(result.raw_file.is_some());

        let raw = fdb.get_file_with_id(result.raw_file.unwrap()).unwrap();

        println!("{}", raw.filename);
        assert!(fdb.get_file_save_path().join(PathBuf::from(raw.filename)).exists());
        assert!(raw.raw_file.is_none());
        assert!(raw.tags == vec!["__raw"]);
    }

    db_test!(files_get_correct_timestamps(fdb) {
        let old_path = PathBuf::from("test/media/10x10_with_timestamp.png");

        let change_timestamp = NaiveDate::from_ymd_opt(2017,1,1).unwrap().and_hms_opt(0,0,0).unwrap();

        let saved_entry = {
            let result = handle_save_request(
                fdb,
                &FileLocation::Unsaved{path: old_path, raw_file: None},
                &vec!(),
                change_timestamp
            );

            assert_matches!(result, Ok(_));
            let result = result.unwrap();

            match result {
                FileSaveRequestResult::NewDatabaseEntry(file_entry, worker_receivers) => {
                    assert_matches!(worker_receivers.file.recv().unwrap(), Ok(()));
                    assert_matches!(worker_receivers
                            .thumbnail
                            .expect("Thumbnail generator channel not created")
                            .recv()
                            .expect("Thumbnail saving failed"), Ok(())
                        );

                    match file_entry {
                        FileLocation::Database(result) => {
                            result
                        }
                        _ => panic!("Unreachable branch"),
                    }
                }
                _ => panic!("Unreachable branch"),
            }
        };

        let query = search::SavedSearchQuery::with_date_constraints(
            date_search::DateConstraints::with_intervals(vec!(
                    date_search::Interval::new(
                        NaiveDate::from_ymd_opt(2018, 5, 31).unwrap().and_hms_opt(20, 39, 0).unwrap(),
                        NaiveDate::from_ymd_opt(2018, 5, 31).unwrap().and_hms_opt(20, 39, 59).unwrap()
                    )))
        );

        println!("{:#?}", saved_entry.id);
        //Make sure that the file was actually added to the database
        for file in fdb.search_files(query) {
            assert_eq!(file.id, saved_entry.id)
        }

        // Make sure a change was added with the correct
        let changes = fdb.get_all_changes()
            .expect("Failed to get changes from database ");

        assert_eq!(changes.len(), 1);
        assert_eq!(changes[0], Change::new(change_timestamp, saved_entry.id, ChangeType::FileAdded));
    });
}
